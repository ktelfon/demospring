package com.example.demo.ex9.service;

import com.example.demo.ex9.exception.SdaException;
import com.example.demo.ex9.model.FileData;
import com.example.demo.ex9.model.FileDataWrap;
import com.example.demo.ex9.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FileDataService {
    private final FileDataRepository fileDataRepository;

    public FileDataWrap getAll() {
        return new FileDataWrap(fileDataRepository.findAll());
    }

    public FileData getById(String fileId) {
        return fileDataRepository
                .findById(fileId)
                .orElseThrow(
                        ()->new SdaException("No such file.")
                );
    }

    public FileData save(FileData fileData) {
        return fileDataRepository.save(fileData);
    }

    public FileData update(String fileId, FileData fileData) {
        FileData fileToUpdate = getById(fileId);
        return fileDataRepository.save(fileData);
    }

    public void delete(String fileId) {
        fileDataRepository.deleteById(fileId);
    }
}
