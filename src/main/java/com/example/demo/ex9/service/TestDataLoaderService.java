package com.example.demo.ex9.service;
import com.example.demo.ex9.model.FileData;
import com.example.demo.ex9.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestDataLoaderService implements CommandLineRunner {

    private final FileDataRepository fileDataRepository;

    @Override
    public void run(String... args) throws Exception {
        fileDataRepository.saveAll(
                List.of(
                        FileData.builder().fileName("First").build(),
                        FileData.builder().fileName("Second").build(),
                        FileData.builder().fileName("Third").build()
                )
        );

        List<FileData> all = fileDataRepository.findAll();
        log.info(String.valueOf(all));
    }
}
