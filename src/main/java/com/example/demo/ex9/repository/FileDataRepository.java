package com.example.demo.ex9.repository;

import com.example.demo.ex9.model.FileData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDataRepository extends JpaRepository<FileData, String> {
}
