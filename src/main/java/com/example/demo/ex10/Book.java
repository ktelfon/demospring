package com.example.demo.ex10;

import lombok.*;

import javax.persistence.*;

@Entity(name = "books")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;
    private String author;
    private String ISBN;
    private int pagesNum;
}
