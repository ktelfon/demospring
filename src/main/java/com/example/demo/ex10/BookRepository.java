package com.example.demo.ex10;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findByTitle(String title);

    List<Book> findByTitleStartingWith(String title);

    Book findByISBN(String isbn);

    Book findByISBNAndAuthor(String isbn, String author);

    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);

    List<Book> findByPagesNumIsBetween(int from, int to);

    @Query(value = "SELECT b FROM books b WHERE b.pagesNum > :x")
    List<Book> findWherePagesNumIsGreaterThanX(@Param("x") Integer x);
}
