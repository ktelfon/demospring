package com.example.demo.register.example.service;

import com.example.demo.register.example.entities.User;
import com.example.demo.register.example.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByLogin(String login);

    User save(UserRegistrationDto registration);
}
