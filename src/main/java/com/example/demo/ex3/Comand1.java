package com.example.demo.ex3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Comand1 implements CommandLineRunner {

    private final DummyLoggerInterface dummyLoggerInterface;

    public Comand1(DummyLoggerInterface dummyLoggerInterface) {
        this.dummyLoggerInterface = dummyLoggerInterface;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLoggerInterface.sayHello();
    }
}
