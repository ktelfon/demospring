package com.example.demo.ex3;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class DummyLoggerImplMain implements DummyLoggerInterface{
    @Override
    public void sayHello() {
        System.out.println(DummyLoggerImplMain.class.getName());
    }
}
