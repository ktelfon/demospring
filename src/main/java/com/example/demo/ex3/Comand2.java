package com.example.demo.ex3;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Comand2 implements CommandLineRunner {

    private final DummyLoggerInterface dummyLoggerInterface;

    public Comand2(@Qualifier("dummyLoggerImpl") DummyLoggerInterface dummyLoggerInterface) {
        this.dummyLoggerInterface = dummyLoggerInterface;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLoggerInterface.sayHello();
    }
}
