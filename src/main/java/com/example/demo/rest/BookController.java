package com.example.demo.rest;

import com.example.demo.ex10.Book;
import com.example.demo.ex10.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;
    private final BookRepository bookRepository;

    // returning all books with a specific title
    @Secured({"ROLE_USER"})
    @GetMapping("/book/{title}")
    public List<Book> generateBook(@PathVariable("title") String title) {
        List<Book> result = bookService.generateAndGetByTitle(title);
        return result;
    }

    // returning the book after ISBN
    @GetMapping("/book/byIsbn/{isbn}")
    public Book getByIsbnBook(@PathVariable("isbn") String isbn) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("asdf")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(234)
                .author("asdf")
                .title("Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        Book result = bookRepository.findByISBN(isbn);
        return result;
    }

    // returning the author's book on a specific ISBN
    @GetMapping("/book/byIsbnAndAuthor/{isbn}/{author}")
    public Book getByIsbnAndAuthor(
            @PathVariable("isbn") String isbn,
            @PathVariable("author") String author) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("Deniss")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(234)
                .author("Not Deniss")
                .title("Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        Book result = bookRepository.findByISBNAndAuthor(isbn, author);
        return result;
    }

    //returning the 3 thickest books by a given author (the one with the most pages should be the first, the last one with the least)
    @GetMapping("/book/author/{author}")
    public List<Book> get3BooksByAuthor(
            @PathVariable("author") String author) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("Not Deniss")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(2343)
                .author("Not Deniss")
                .title("Big bad book")
                .build();
        Book book3 = Book.builder()
                .ISBN("456")
                .pagesNum(23443)
                .author("Not Deniss")
                .title("Big bad book")
                .build();
        Book book4 = Book.builder()
                .ISBN("456")
                .pagesNum(234567)
                .author("Not Deniss")
                .title("Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2, book3, book4));
        List<Book> result = bookRepository.findTop3ByAuthorOrderByPagesNumDesc(author);
        return result;
    }


    // that returns all books whose names begin with a particular String
    @GetMapping("/book/getLikeTitle/{title}")
    public List<Book> getLikeTitle(
            @PathVariable("title") String title) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("Deniss")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(234)
                .author("Not Deniss")
                .title("Not Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        List<Book> result = bookRepository.findByTitleStartingWith(title);
        return result;
    }

    // returning all books with pages between the two values
    @GetMapping("/book/between/{min}/{max}")
    public List<Book> getBookBetween(
            @PathVariable("min") int min,
            @PathVariable("max") int max) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("Deniss")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(2344)
                .author("Not Deniss")
                .title("Not Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        List<Book> result = bookRepository.findByPagesNumIsBetween(min, max);
        return result;
    }

    // In addition, implement a method called findWherePagesNumIsGreaterThanX and one argument of typeInteger that
    // returns all books that have at least as many pages as the input argument is.
    @GetMapping("/book/greater/{x}")
    public List<Book> getBookGreater(
            @PathVariable("x") int x) {
        Book book1 = Book.builder()
                .ISBN("123")
                .pagesNum(234)
                .author("Deniss")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("456")
                .pagesNum(2344)
                .author("Not Deniss")
                .title("Not Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        List<Book> result = bookRepository.findWherePagesNumIsGreaterThanX(x);
        return result;
    }
}
