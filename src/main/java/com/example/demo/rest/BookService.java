package com.example.demo.rest;

import com.example.demo.ex10.Book;
import com.example.demo.ex10.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public List<Book> generateAndGetByTitle(String title){
        Book book1 = Book.builder()
                .ISBN("")
                .pagesNum(234)
                .author("asdf")
                .title("Big bad book")
                .build();
        Book book2 = Book.builder()
                .ISBN("")
                .pagesNum(234)
                .author("asdf")
                .title("Big bad book")
                .build();
        bookRepository.saveAll(List.of(book1, book2));
        return bookRepository.findByTitle(title);
    }
}
